package ch.wellengang

class Meeting {

    String title
    Room room

    static namedQueries = {
        filterOnRoom { int roomNumber ->
            room {
                eq 'number', roomNumber
            }
        }
    }
}
