package ch.wellengang

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Meeting)
@Mock(Room)
class MeetingSpec extends Specification {

    def setup() {
        Room room1 = new Room(number: 20, floor: 1).save(flush: true, failOnError: true)
        Room room2 = new Room(number: 21, floor: 1).save(flush: true, failOnError: true)
        new Meeting(title: "Meeting 1", room: room1).save(flush: true, failOnError: true)
        new Meeting(title: "Meeting 2", room: room2).save(flush: true, failOnError: true)
    }

    def cleanup() {
    }

    void "Filter by Room"() {
        when:
            List meetings = Meeting.filterOnRoom(20).list()

        then:
            meetings.size() == 1
    }
}
